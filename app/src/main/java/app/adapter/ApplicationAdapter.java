package app.adapter;

import android.net.Uri;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import app.struct.App;
import core.G;
import ir.vahidgarousi.app.androidmarket.R;

/**
 * Created by developer on 3/24/2018
 */

public class ApplicationAdapter extends RecyclerView.Adapter<ApplicationAdapter.ApplicationViewHolder> {

    private ArrayList<App> apps = new ArrayList<>();
    private int resourceId = -1;
    private static ApplicationViewHolder.onApplicationClick onApplicationClick;

    public ApplicationAdapter(ArrayList<App> apps, ApplicationViewHolder.onApplicationClick onApplicationClick) {
        this.apps = apps;
        this.onApplicationClick = onApplicationClick;
    }

    public ApplicationAdapter(ApplicationAdapter.ApplicationViewHolder.onApplicationClick onApplicationClick, @LayoutRes int resourceId) {
        this.onApplicationClick = onApplicationClick;
        this.resourceId = resourceId;
    }

    public void addApplication(ArrayList<App> apps) {
        this.apps.addAll(apps);
        notifyDataSetChanged();
    }

    @Override
    public ApplicationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = G.layoutInflater.inflate(resourceId == -1 ? R.layout.item_application : resourceId, parent, false);
        return new ApplicationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ApplicationViewHolder holder, int position) {
        holder.bindApplications(apps.get(position));
    }

    @Override
    public int getItemCount() {
        return apps.size();
    }

    public static class ApplicationViewHolder extends RecyclerView.ViewHolder {
        private ImageView appImage;
        private TextView appName;
        private TextView appPrice;

        public ApplicationViewHolder(View itemView) {
            super(itemView);
            appImage = itemView.findViewById(R.id.iv_itemApp_image);
            appName = itemView.findViewById(R.id.tv_itemApp_name);
            appPrice = itemView.findViewById(R.id.tv_itemApp_price);
            appName.setTypeface(G.typefaceIranSansLight);
            appPrice.setTypeface(G.typefaceIranSansLight);
        }

        public void bindApplications(final App app) {
            appName.setText(app.getName());
            Picasso.get().load(Uri.parse(app.getLogoUrl())).into(appImage);
            appPrice.setText(app.getPrice());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onApplicationClick.onAppClick(app);
                }
            });
        }

        public interface onApplicationClick {
            void onAppClick(App app);
        }
    }
}
