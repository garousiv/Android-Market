package main.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import app.adapter.ApplicationAdapter;
import app.struct.App;
import core.G;
import ir.vahidgarousi.app.androidmarket.R;
import main.activity.ImagePagerAdapter;

/**
 * Created by developer on 3/27/2018
 */

public class ApplicationDetailFragment extends FragmentActivity implements ApplicationAdapter.ApplicationViewHolder.onApplicationClick {


    private static final String TAG = "ApplicationDetailFragme";
    private ViewPager viewPager;
    private TextView appNameTextView;
    private TextView appManufacturerTextView;
    private TextView appInstalledCountNumberTextView;
    private TextView appInstalledCountUnitTextView;
    private TextView appPonitsNumberTextView;
    private TextView appCommentsCountTextView;
    private ImageView appCategoryImageImageView;
    private ImageView appLogo;
    private TextView appCategoryTitleTextView;
    private TextView appFileSizeNumberTextView;
    private TextView appCapacityHeaderTextView;
    private TextView appEarcnMoney;
    private TextView appInstalledHeaderTextView;
    private TextView appCapacityUnitTextView;
    private TextView appSmallDescriptionTextView;
    private TextView appMoreDescriptionTextView;
    private TextView appAddCoteToThisAppTextView;
    private TextView appCommentsUserNameTextView;
    private TextView appCommentUserTextTextView;
    private TextView appCommentUserVersionTextView;
    private static LinearLayout detailContainerLinearLayout;
    private static ProgressBar detailContainerProgressbar;
    private ImageView appFragmentBackButton;
    private static boolean isDataRecievied = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_app_detail);
        setupViews();
        checkDataReceived();
        getAppImages();
        backButtonListener();
    }


    private void backButtonListener() {
        appFragmentBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent intent = new Intent(ApplicationDetailFragment.this, MainActivity.class);
                // startActivity(intent);
                finish();
            }
        });
    }

    private void getAppImages() {
        ArrayList<Integer> imageIds = new ArrayList<>();
        ArrayList<String> imageTitles = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            int imageId = G.context.getResources().getIdentifier("image" + i, "drawable", G.context.getPackageName());
            imageIds.add(imageId);
            imageTitles.add("عکس خوشگل#" + i);
        }
        ImagePagerAdapter adapter = new ImagePagerAdapter(imageIds, imageTitles);
        viewPager.setAdapter(adapter);
    }

    private void checkDataReceived() {
        if (G.app instanceof App) {
            isDataRecievied = true;
        } else {
            isDataRecievied = false;

        }
        if (isDataRecievied) {
            detailContainerLinearLayout.setVisibility(View.VISIBLE);
            detailContainerProgressbar.setVisibility(View.GONE);
            initialize();
        } else {
            detailContainerLinearLayout.setVisibility(View.GONE);
            detailContainerProgressbar.setVisibility(View.VISIBLE);
        }
        Toast.makeText(this, "I am checkDataReceived Method!", Toast.LENGTH_SHORT).show();
    }

    private void initialize() {
        Toast.makeText(this, "initialize" + G.app.getName(), Toast.LENGTH_SHORT).show();
        Picasso.get().load(Uri.parse(G.app.getLogoUrl())).into(appLogo);
        appNameTextView.setText(G.app.getName());
        appManufacturerTextView.setText(G.app.getAuthor());
        appInstalledCountNumberTextView.setText(G.app.getInstalls());
        appPonitsNumberTextView.setText(G.app.getVotes());
        Picasso.get().load(Uri.parse(G.app.getLogoUrl())).into(appCategoryImageImageView);
        appFileSizeNumberTextView.setText(G.app.getFileSize());
        appCommentsCountTextView.setText(G.app.getVotes());
        appSmallDescriptionTextView.setText(G.app.getDescription().substring(0, 40));

    }

    private void setupViews() {
        viewPager = findViewById(R.id.bs_appDetailFragment_appImages);

        appNameTextView = findViewById(R.id.tv_appDetailFragment_Name);
        appManufacturerTextView = findViewById(R.id.tv_appDetailFragment_Manufacturer);
        appInstalledCountNumberTextView = findViewById(R.id.tv_appDetailFragment_installCount);
        appInstalledCountUnitTextView = findViewById(R.id.tv_appDetailFragment_installCountUnit);
        appPonitsNumberTextView = findViewById(R.id.tv_appDetailFragment_appPoints);
        appCommentsCountTextView = findViewById(R.id.tv_appDetailFragment_appCommentsCount);
        appCategoryImageImageView = findViewById(R.id.iv_appDetailFragment_appCategoryImage);
        appCategoryTitleTextView = findViewById(R.id.tv_appDetailFragment_appCategoryName);
        appFileSizeNumberTextView = findViewById(R.id.tv_appDetailFragment_appSizeNumber);
        appCapacityHeaderTextView = findViewById(R.id.iv_appDetailFragment_Volume);
        appEarcnMoney = findViewById(R.id.iv_appDetailFragment_AppType);
        appCapacityUnitTextView = findViewById(R.id.tv_appDetailFragment_appSizeUnit);
        appSmallDescriptionTextView = findViewById(R.id.tv_appDetailFragment_description);
        appMoreDescriptionTextView = findViewById(R.id.tv_appDetailFragment_more);
        appAddCoteToThisAppTextView = findViewById(R.id.tv_appDetailFragment_voteToThisApp);
        appCommentsUserNameTextView = findViewById(R.id.tv_appDetailFragment_userThatAddCommentName);
        appInstalledHeaderTextView = findViewById(R.id.iv_appDetailFragment_countInstallHeader);
        appCommentUserTextTextView = findViewById(R.id.iv_appDetailFragment_userThatAddCommentText);
        appCommentUserVersionTextView = findViewById(R.id.iv_appDetailFragment_userCommentAppVersion);
        appFragmentBackButton = findViewById(R.id.iv_appDetailFragment_back);
        detailContainerLinearLayout = findViewById(R.id.ln_fragmentActivity_detailContainer);
        detailContainerProgressbar = findViewById(R.id.prg_fragmentActivity_progressBar);
        appLogo = findViewById(R.id.iv_appDetailFragment_image);


        appNameTextView.setTypeface(G.typefaceIranSansLight);
        appManufacturerTextView.setTypeface(G.typefaceIranSansLight);
        appInstalledCountNumberTextView.setTypeface(G.typefaceIranSansLight);
        appInstalledCountUnitTextView.setTypeface(G.typefaceIranSansLight);
        appPonitsNumberTextView.setTypeface(G.typefaceIranSansLight);
        appCommentsCountTextView.setTypeface(G.typefaceIranSansLight);
        appCategoryTitleTextView.setTypeface(G.typefaceIranSansLight);
        appFileSizeNumberTextView.setTypeface(G.typefaceIranSansLight);
        appMoreDescriptionTextView.setTypeface(G.typefaceIranSansLight);
        appCapacityUnitTextView.setTypeface(G.typefaceIranSansLight);
        appEarcnMoney.setTypeface(G.typefaceIranSansLight);
        appSmallDescriptionTextView.setTypeface(G.typefaceIranSansLight);
        appInstalledHeaderTextView.setTypeface(G.typefaceIranSansLight);
        appSmallDescriptionTextView.setTypeface(G.typefaceIranSansLight);
        appAddCoteToThisAppTextView.setTypeface(G.typefaceIranSansLight);
        appCommentsUserNameTextView.setTypeface(G.typefaceIranSansLight);
        appCommentUserTextTextView.setTypeface(G.typefaceIranSansLight);
        appCommentUserVersionTextView.setTypeface(G.typefaceIranSansLight);
    }

    public static void start(ApplicationAdapter.ApplicationViewHolder.onApplicationClick onApplicationClick, App app) {
        Toast.makeText(G.context, app.getName(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(G.currentActivity, ApplicationDetailFragment.class);
        intent.putExtra("app", app);
        G.currentActivity.startActivity(intent);
    }

    @Override
    public void onAppClick(App app) {

    }
}
