package main.activity;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;


import java.util.ArrayList;
import java.util.List;

import app.struct.App;
import banners.Banner;
import NavigationCategory.fragment.BestAppFragment;
import NavigationCategory.fragment.CategoryFragment;
import NavigationCategory.fragment.HomeFragment;
import common.UTab;
import core.G;
import ir.vahidgarousi.app.androidmarket.R;
import NavigationCategory.fragment.MyAppFragment;
import NavigationCategory.fragment.SearchFragment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    // private BottomNavigationBar bnv_main;
    private ArrayList<App> apps = new ArrayList<>();
    final private List<Banner> banners = new ArrayList<>();
    private FrameLayout fragmentContainer;
    private int lastTab = 1;
    private TabLayout tabLayout;
    private ProgressBar progressBarMain;
    private LinearLayout loadingDataLineaLayout;
    private ViewPager viewPager;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupViews();
//        if (G.app instanceof App) {
//            viewPager.setVisibility(View.VISIBLE);
//            loadingDataLineaLayout.setVisibility(View.GONE);
//        } else {
//            viewPager.setVisibility(View.GONE);
//            loadingDataLineaLayout.setVisibility(View.VISIBLE);
//        }
        init2();

        //  TabLayout.Tab tab = tabLayout.getTabAt(1);
        // tab.select();
    }



    private void resetState() {
        if (G.app instanceof App) {
            viewPager.setVisibility(View.VISIBLE);
            loadingDataLineaLayout.setVisibility(View.GONE);
        } else {
            viewPager.setVisibility(View.GONE);
            loadingDataLineaLayout.setVisibility(View.VISIBLE);
        }
    }



    private void init2() {
        tabLayout = findViewById(R.id.tl_main_tab);
        //tl_mainActivity_tab.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
        UTab uTab = new UTab(this, R.id.vp_main_fragmentContainer, R.id.tl_main_tab);
        final ViewPager viewPager = findViewById(R.id.vp_main_fragmentContainer);
        final TabLayout tabLayout = findViewById(R.id.tl_main_tab);
        uTab.add(HomeFragment.class, R.drawable.ic_home_gray_24dp, "خانه");
        uTab.add(BestAppFragment.class, R.drawable.ic_best_app_gray_24dp, "برترینها");
        uTab.add(CategoryFragment.class, R.drawable.ic_menu_gray_24dp, "دسته ها");
        uTab.add(SearchFragment.class, R.drawable.ic_search_gray_24dp, "جستجو");
        uTab.add(MyAppFragment.class, R.drawable.ic_apps_gray_24dp, "برنامه ها");

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition(), false);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void selectFragment(int position) {
        switch (position) {
            case 0:
                setFragment(new HomeFragment());
                break;
            case 1:
                setFragment(new BestAppFragment());
                break;
            case 2:
                setFragment(new CategoryFragment());
                break;
            case 3:
                setFragment(new SearchFragment());
                break;
            case 4:
                setFragment(new MyAppFragment());
                break;
        }
    }

    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.vp_main_fragmentContainer, fragment);
        fragmentTransaction.commit();
    }

    public static void setFragment(Fragment fragment, FragmentManager fragmentManager) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.vp_main_fragmentContainer, fragment);
        fragmentTransaction.commit();
    }
//    private void init() {
//        bnv_main
//                .addItem(new BottomNavigationItem(R.drawable.ic_home_gray_24dp, "صفحه اصلی"))
//                .addItem(new BottomNavigationItem(R.drawable.ic_best_app_gray_24dp, "برترین ها"))
//                .addItem(new BottomNavigationItem(R.drawable.ic_menu_gray_24dp, "دسته ها"))
//                .addItem(new BottomNavigationItem(R.drawable.ic_search_gray_24dp, "جستجو"))
//                .addItem(new BottomNavigationItem(R.drawable.ic_apps_gray_24dp, "برنامه های من"))
//                .initialise();
//
//        selectFragment(bnv_main.getCurrentSelectedPosition());
//        bnv_main.setTabSelectedListener(new BottomNavigationBar.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(int position) {
//                Log.i(TAG, "onTabSelected: " + position);
//                selectFragment(position);
//            }
//
//            @Override
//            public void onTabUnselected(int position) {
//
//            }
//
//            @Override
//            public void onTabReselected(int position) {
//
//            }
//        });
//    }

//    private void selectFragment(int position) {
//        switch (position) {
//            case 0:
//                setFragment(new HomeFragment());
//                break;
//            case 1:
//                setFragment(new BestAppFragment());
//                break;
//            case 2:
//                setFragment(new CategoryFragment());
//                break;
//            case 3:
//                setFragment(new SearchFragment());
//                break;
//            case 4:
//                setFragment(new MyAppFragment());
//                break;
//        }
//    }

//    private void setFragment(Fragment fragment) {
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.fl_main_fragmentContainer, fragment);
//        fragmentTransaction.commit();
//    }

    private void setupViews() {
        progressBarMain = findViewById(R.id.prg_mainActivity_progressBar);
        loadingDataLineaLayout = findViewById(R.id.ln_loadingData);
        viewPager = findViewById(R.id.vp_main_fragmentContainer);
        //  fragmentContainer = findViewById(R.id.fl_main_fragmentContainer);
        //bnv_main = findViewById(R.id.tl_main_tab);
    }

//
//    public void showApplicationDetail(Fragment fragment) {
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        MainActivity.setFragment(fragment);
//    }
}
