package main.activity;

import java.util.ArrayList;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import core.G;
import ir.vahidgarousi.app.androidmarket.R;


public class ImagePagerAdapter extends PagerAdapter {

    public ArrayList<Integer> imageIds;
    public ArrayList<String>  imageTitles;


    public ImagePagerAdapter(ArrayList<Integer> imageIds, ArrayList<String> imageTitles) {
        this.imageIds = imageIds;
        this.imageTitles = imageTitles;
    }


    @Override
    public int getCount() {
        return imageIds.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = G.layoutInflater.inflate(R.layout.sample, null);
        ImageView image = (ImageView) view.findViewById(R.id.imageViewPage);
        image.setImageResource(imageIds.get(position));
        container.addView(view);
        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
