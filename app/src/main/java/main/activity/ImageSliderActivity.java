package main.activity;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

import core.G;
import ir.vahidgarousi.app.androidmarket.R;

public class ImageSliderActivity extends AppCompatActivity {

    private static final String TAG = "ImageSliderActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_slider);

        final PageIndicator indicator = findViewById(R.id.indicator);

        ArrayList<Integer> imageIds = new ArrayList<>();
        ArrayList<String> imageTitles = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            int imageId = G.context.getResources().getIdentifier("image" + i, "drawable", G.context.getPackageName());
            imageIds.add(imageId);
            imageTitles.add("عکس خوشگل#" + i);
        }
        indicator.setIndicatorsCount(imageTitles.size());
        ViewPager pager = findViewById(R.id.viewPager);

        ImagePagerAdapter adapter = new ImagePagerAdapter(imageIds, imageTitles);
        pager.setAdapter(adapter);
    }
}
