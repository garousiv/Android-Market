package NavigationCategory.Views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import NavigationCategory.adapter.BestAppAdapterSimple;
import app.struct.App;
import core.G;
import ir.vahidgarousi.app.androidmarket.R;

/**
 * Created by developer on 3/24/2018
 */

public class BestAppView extends LinearLayout {
    private RecyclerView itemBestFragmentRecyclerView;
    private ImageView itemBestFragmentBanner;

    public BestAppView(Context context) {
        super(context);
        init();
    }

    public BestAppView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BestAppView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public BestAppView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        View view = G.layoutInflater.inflate(R.layout.item_best_fragment, this, true
        );
        itemBestFragmentRecyclerView = view.findViewById(R.id.rv_itemBestFragment_bestFragment_recyclerView);
       // itemBestFragmentBanner = view.findViewById(R.id.iv_itemBestFragment_banner);
    }

    public void attachApplications(ArrayList<App> applications, BestAppAdapterSimple.BestAppViewHolder.onAppClick onAppClick){

        itemBestFragmentRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        BestAppAdapterSimple bestAppAdapterSimple = new BestAppAdapterSimple(applications,onAppClick);
     //   bestAppAdapterSimple.addApplication(applications);
        itemBestFragmentRecyclerView.setAdapter(bestAppAdapterSimple);
    }
}
