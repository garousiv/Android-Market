package NavigationCategory.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import apiservice.ApiService;
import app.adapter.ApplicationAdapter;
import app.struct.App;
import banners.Banner;
import banners.RemoteBanner;
import core.G;
import events.OnBannerClickListener;
import ir.vahidgarousi.app.androidmarket.R;
import NavigationCategory.Views.AppRowView;
import main.activity.MainActivity;
import main.fragment.ApplicationDetailFragment;
import views.BannerSlider;


/**
 * Created by developer on 3/23/2018
 */

public class HomeFragment extends Fragment {


    private static final String TAG = "HomeFragment";

    private AppRowView appRowRecentAppView;
    private AppRowView appRowViewRecentGame;
    private View rootView;
    private BannerSlider bannerSliderMainFragment;
    private List<Banner> banners;
    private ApplicationAdapter.ApplicationViewHolder.onApplicationClick onApplicationClick = new ApplicationAdapter.ApplicationViewHolder.onApplicationClick() {
        @Override
        public void onAppClick(App app) {
            G.app = app;
            ApplicationDetailFragment.start(this, G.app);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            Log.i(TAG, "Fragment onCreateView");
            rootView = inflater.inflate(R.layout.fragment_main, container, false);
            setupViews();
            init();
            getApplications();
        }
        return rootView;
    }

    private void getApplications() {
        ApiService apiService = new ApiService(G.context);
        apiService.getApplication(new Response.Listener<ArrayList<App>>() {
            @Override
            public void onResponse(ArrayList<App> response) {
                Log.d(TAG, "onResponse() called with: response = [" + response + "]");
                appRowRecentAppView.attachApplications(response, onApplicationClick);
                appRowViewRecentGame.attachApplications(response, onApplicationClick);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse() called with: error = [" + error + "]");
                Log.i(TAG, "onErrorResponse: " + error.getMessage());
            }
        });
    }


    private void init() {
        getAndShowBanners(bannerSliderMainFragment);
    }

    public void setupViews() {
        appRowRecentAppView = rootView.findViewById(R.id.appRow_main_recentApp);
        appRowViewRecentGame = rootView.findViewById(R.id.appRow_main_recentGame);
        bannerSliderMainFragment = rootView.findViewById(R.id.bannerSlider_main_top);
        bannerSliderMainFragment.setOnBannerClickListener(new OnBannerClickListener() {
            @Override
            public void onClick(int position) {
                Toast.makeText(getContext(), "Banner with position " + String.valueOf(position) + " clicked!", Toast.LENGTH_SHORT).show();
            }
        });
        banners = new ArrayList<>();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        G.currentActivity = getActivity();
    }

    private void getAndShowBanners(BannerSlider bannerSlider) {
        //add banner using image url
        RemoteBanner remoteBanner1 = new RemoteBanner("https://s.cafebazaar.ir/3/splashes/splash1281_xl_fa.jpg");
        RemoteBanner remoteBanner2 = new RemoteBanner("https://s.cafebazaar.ir/3/splashes/splash1281_xl_fa.jpg");
        RemoteBanner remoteBanner3 = new RemoteBanner("https://s.cafebazaar.ir/3/splashes/splash1287_xl_fa.jpg");
        RemoteBanner remoteBanner4 = new RemoteBanner("https://s.cafebazaar.ir/3/splashes/splash1277_xl_fa.jpg");
        banners.add(remoteBanner1);
        banners.add(remoteBanner2);
        banners.add(remoteBanner3);
        banners.add(remoteBanner4);
        //add banner using resource drawable
        //banners.add(new DrawableBanner(R.drawable.yourDrawable));
        bannerSlider.setBanners(banners);
    }
}
