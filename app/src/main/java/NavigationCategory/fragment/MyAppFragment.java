package NavigationCategory.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;

import NavigationCategory.adapter.MyAppAdapter;
import app.adapter.ApplicationAdapter;
import app.struct.App;
import core.G;
import ir.vahidgarousi.app.androidmarket.R;
import main.fragment.ApplicationDetailFragment;

/**
 * Created by developer on 3/24/2018
 */

public class MyAppFragment extends Fragment implements
        MyAppAdapter.MyAdapterViewHolder.OnApplicationClickListener,
        ApplicationAdapter.ApplicationViewHolder.onApplicationClick {

    private View rootView;
    private ImageView itemAppImage;
    private TextView itemAppName;
    private Button itemAppRun;
    private RecyclerView appsRecyclerView;
    private static final String TAG = "MyAppFragment";
    private MyAppAdapter myAppAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_my_apps, container, false);
            setupViews();
            init();
        }
        return rootView;
    }

    private void init() {
        appsRecyclerView.setAdapter(myAppAdapter);
        G.apiService.getApplication(new Response.Listener<ArrayList<App>>() {
            @Override
            public void onResponse(ArrayList<App> response) {
                myAppAdapter.attachApplications(response, MyAppFragment.this);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
//        MyAppAdapter myAppAdapter = new MyAppAdapter(G.generateApps(),
//                new MyAppAdapter.MyAdapterViewHolder.OnApplicationClickListener() {
//                    @Override
//                    public void onAppClickListener(App app) {
//                        Toast.makeText(getContext(), "Clicked" + app.getName(), Toast.LENGTH_SHORT).show();
//                    }
//                });
        appsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        appsRecyclerView.setNestedScrollingEnabled(false);
    }

    private void setupViews() {
        itemAppImage = rootView.findViewById(R.id.iv_itemMyApps_appImage);
        itemAppName = rootView.findViewById(R.id.tv_itemMyApps_appName);
        itemAppRun = rootView.findViewById(R.id.btn_itemMyApps_runApp);
        appsRecyclerView = rootView.findViewById(R.id.rv_myAppsFragment_recyclerView);
        myAppAdapter = new MyAppAdapter();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onAppClickListener(App app) {
        G.app = app;
        ApplicationDetailFragment.start(this, G.app);
    }

    @Override
    public void onAppClick(App app) {
        G.app = app;
        ApplicationDetailFragment.start(this, G.app);
    }
}
