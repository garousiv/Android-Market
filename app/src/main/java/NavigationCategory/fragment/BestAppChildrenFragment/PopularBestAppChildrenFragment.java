package NavigationCategory.fragment.BestAppChildrenFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;

import NavigationCategory.Views.BestAppView;
import NavigationCategory.adapter.BestAppAdapterSimple;
import app.adapter.ApplicationAdapter;
import app.struct.App;
import core.G;
import ir.vahidgarousi.app.androidmarket.R;
import main.fragment.ApplicationDetailFragment;

/**
 * Created by developer on 3/25/2018
 */

public class PopularBestAppChildrenFragment extends Fragment implements
        BestAppAdapterSimple.BestAppViewHolder.onAppClick,
        ApplicationAdapter.ApplicationViewHolder.onApplicationClick {
    private View rootView;
    private BestAppView bestAppView;
    private static final String TAG = "PopularBestAppChildrenF";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.view_populer_best_app_children_fragment, container, false);
            setupView();
            init();
        }
        return rootView;
    }

    public static ArrayList<App> generateApps() {

        ArrayList<App> appArrayList = new ArrayList<>();
//        App banner1 = new App();
//        banner1.setImage("https://s.cafebazaar.ir/3/splashes/splash1274_xl_fa.jpg");
//        banner1.setName("برنامه ");
//        banner1.setPrice("رایگان ");
//        banner1.setHeader(true);
//
//        App banner2 = new App();
//        banner2.setImage("https://s.cafebazaar.ir/3/splashes/splash1274_xl_fa.jpg");
//        banner2.setHeader(true);
//        banner2.setName("برنامه ");
//        banner2.setPrice("رایگان ");
//
//        appArrayList.add(banner1);
//        appArrayList.add(banner2);

        for (int i = 0; i <= 2; i++) {
            App app = new App();
            app.setName("برنامه " + i);
            app.setPrice("رایگان " + i);
            app.setLogoUrl("https://s.cafebazaar.ir/1/icons/com.PaeezanStudio.NanJoon_128x128.png");
            appArrayList.add(app);
        }
        return appArrayList;
    }

    private void init() {
        G.apiService.getApplication(new Response.Listener<ArrayList<App>>() {
            @Override
            public void onResponse(ArrayList<App> response) {
                bestAppView.attachApplications(response, PopularBestAppChildrenFragment.this);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
    }

    private void setupView() {
        bestAppView = rootView.findViewById(R.id.bav_fragmentBest);

    }

    @Override
    public void onAppClick(App app) {
        G.app = app;
        ApplicationDetailFragment.start(this, G.app);
    }

    @Override
    public void onAppClicked(App app) {
        G.app = app;
        ApplicationDetailFragment.start(this, G.app);
    }
}
