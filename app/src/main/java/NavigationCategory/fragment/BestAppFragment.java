package NavigationCategory.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import NavigationCategory.fragment.BestAppChildrenFragment.PopularBestAppChildrenFragment;
import common.UTab;
import ir.vahidgarousi.app.androidmarket.R;

/**
 * Created by developer on 3/24/2018
 */

public class BestAppFragment extends Fragment {
    private static final String TAG = "BestAppFragment";
    private View rootView;
    private TabLayout tabLayoutTopMenu;
    private ImageView bannerImageView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_best, container, false);
            setupViews();
            init();
        }
        return rootView;
    }

    private void init() {

        final UTab uTab = new UTab(rootView, getActivity().getSupportFragmentManager(), R.id.vp_bestFragment, R.id.tl_bestFragment_tab);
        uTab.add(PopularBestAppChildrenFragment.class, "پروفروش ها");
        uTab.add(PopularBestAppChildrenFragment.class, "فارسی ها");
        uTab.add(PopularBestAppChildrenFragment.class, "پرطرفدار ها");
//        Picasso.get().load(Uri.parse("https://s.cafebazaar.ir/3/splashes/splash1274_xl_fa.jpg")).into(bannerImageView);

    }

    public void setupViews() {
        tabLayoutTopMenu = rootView.findViewById(R.id.tl_bestFragment_tab);
      //  bannerImageView = rootView.findViewById(R.id.iv_itemBestFragment_banner);
    }


}
