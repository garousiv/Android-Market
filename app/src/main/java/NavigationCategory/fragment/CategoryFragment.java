package NavigationCategory.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;

import app.adapter.ApplicationAdapter;

import app.struct.App;
import core.G;
import ir.vahidgarousi.app.androidmarket.R;
import NavigationCategory.Views.AppRowView;
import main.fragment.ApplicationDetailFragment;

/**
 * Created by developer on 3/24/2018
 */

public class CategoryFragment extends Fragment implements
        ApplicationAdapter.ApplicationViewHolder.onApplicationClick {

    private static final String TAG = "CategoryFragment";
    private View rootView;
    private TextView categoryTitleTextView;
    private AppRowView appRowViewCategory1;
    private AppRowView appRowViewCategory2;
    private AppRowView appRowViewCategory3;
    private AppRowView appRowViewCategory4;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_category, container, false);
            setupViews();
            init();
        }
        return rootView;
    }

    private void init() {
        G.apiService.getApplication(new Response.Listener<ArrayList<App>>() {
            @Override
            public void onResponse(ArrayList<App> response) {
                 appRowViewCategory1.attachApplications(response,CategoryFragment.this);
                 appRowViewCategory2.attachApplications(response,CategoryFragment.this);
                 appRowViewCategory3.attachApplications(response,CategoryFragment.this);
                 appRowViewCategory4.attachApplications(response,CategoryFragment.this);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
    }

    public void setupViews() {
        categoryTitleTextView = rootView.findViewById(R.id.tv_categoryFragment_title);
        appRowViewCategory1 = rootView.findViewById(R.id.appRow_categoryFragment_category1);
        appRowViewCategory2 = rootView.findViewById(R.id.appRow_categoryFragment_category2);
        appRowViewCategory3 = rootView.findViewById(R.id.appRow_categoryFragment_category3);
        appRowViewCategory4 = rootView.findViewById(R.id.appRow_categoryFragment_category4);
    }

    @Override
    public void onAppClick(App app) {
        G.app = app;
        ApplicationDetailFragment.start(this, G.app);
    }
}
