package NavigationCategory.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import app.struct.App;
import core.G;
import ir.vahidgarousi.app.androidmarket.R;

/**
 * Created by developer on 3/25/2018
 */

public class MyAppAdapter extends RecyclerView.Adapter<MyAppAdapter.MyAdapterViewHolder> {

    public MyAppAdapter() {
    }

    private List<App> apps = new ArrayList<>();
    private static MyAdapterViewHolder.OnApplicationClickListener onAppClick;

    public MyAppAdapter(List<App> apps, MyAdapterViewHolder.OnApplicationClickListener onAppClick) {
        this.apps = apps;
        MyAppAdapter.onAppClick = onAppClick;
    }

    @Override
    public MyAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = G.layoutInflater.inflate(R.layout.item_my_apps, parent, false);
        return new MyAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyAdapterViewHolder holder, int position) {
        holder.bindApplications(apps.get(position));
    }

    @Override
    public int getItemCount() {
        return apps.size();
    }

    public void attachApplications(ArrayList<App> applicationList, MyAdapterViewHolder.OnApplicationClickListener onApplicationClickListener) {
        this.apps = applicationList;
        this.onAppClick = onApplicationClickListener;
        notifyDataSetChanged();
    }

    public static class MyAdapterViewHolder extends RecyclerView.ViewHolder {
        private ImageView appImageImageView;
        private TextView appNameTextView;
        private Button runAppButton;

        public MyAdapterViewHolder(View itemView) {
            super(itemView);
            appImageImageView = itemView.findViewById(R.id.iv_itemMyApps_appImage);
            appNameTextView = itemView.findViewById(R.id.tv_itemMyApps_appName);
            runAppButton = itemView.findViewById(R.id.btn_itemMyApps_runApp);
            runAppButton.setTypeface(G.typefaceIranSansLight);
            appNameTextView.setTypeface(G.typefaceIranSansLight);
        }

        public void bindApplications(final App app) {
            if (!app.getName().isEmpty() && !app.getLogoUrl().isEmpty()) {
                appNameTextView.setText(app.getName());
                Picasso.get().load(Uri.parse(app.getLogoUrl())).into(appImageImageView);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAppClick.onAppClickListener(app);
                    }
                });
            }
        }

        public interface OnApplicationClickListener {
            void onAppClickListener(App app);
        }
    }
}
