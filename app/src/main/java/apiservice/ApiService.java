package apiservice;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import app.struct.App;


/**
 * Created by vahid on 3/16/2018
 */

public class ApiService {

    private static final String TAG = "LOG";
    private static final String BASE_URL = "http://192.168.1.100/market-server/service.php?action=";
    private static final String ENDPOINT_PRODUCTS = "read";
    private static final String ENDPOINT_COMMENT = "comments";
    private Context context;

    public ApiService(Context context) {
        this.context = context;
    }

    public void getApplication(int page, int order,
                               Response.Listener<ArrayList<App>> listener,
                               Response.ErrorListener errorListener) {
        GsonRequest<ArrayList<App>> gsonRequest = new GsonRequest<>(context, Request.Method.GET,
                BASE_URL + ENDPOINT_PRODUCTS,
                null,
                listener,
                errorListener,
                new TypeToken<ArrayList<App>>() {
                }.getType()
        );

        Volley.newRequestQueue(context).add(gsonRequest);
    }

    public void getApplication(final int page,
                               Response.Listener<ArrayList<App>> listener,
                               Response.ErrorListener errorListener) {
        getApplication(page, App.DEFAULT_SORT, listener, errorListener);
    }

    public void getApplication(
            Response.Listener<ArrayList<App>> listener,
            Response.ErrorListener errorListener, int order) {

        getApplication(1, order, listener, errorListener);
    }

    public void getApplication(
            Response.Listener<ArrayList<App>> listener,
            Response.ErrorListener errorListener) {
        getApplication(1, App.DEFAULT_SORT, listener, errorListener);
    }

//    public void getComment(String product_id, Response.Listener<ArrayList<Comment>> listener,
//                           Response.ErrorListener errorListener) {
//        GsonRequest<ArrayList<Comment>> gsonRequest = new GsonRequest<>(Request.Method.POST,
//                BASE_URL + ENDPOINT_COMMENT,
//                null,
//                listener,
//                errorListener,
//                new TypeToken<ArrayList<Comment>>() {
//                }.getType()
//        );
//    }
}
